#!/usr/bin/env python
import bioutils
import sys
import argparse
import os


#argument parsing
options = argparse.ArgumentParser(description="applies RNASeq processing pipeline to one or more fastq files.")

options.add_argument("folderName", help='the name of the folder in which all the analyses will be carried out. the folder will be created in the directory where this program was executed.')
options.add_argument("fastqFile", nargs="+", help= "Path to one or more fastq or fastq.gz files")
options.add_argument('-N', '--normfactor', metavar='N', default=10000000, type=int, help='specifies the normalization factor when normalizing wig files by library size. default is 10,000,000.' )
options.add_argument('-g', '--gff', metavar='file.gff', default='', help='specify path to the GFF file to be used in HTSeq-count. if nothing is specified, HTSeq-count will not be performed.')
options.add_argument('-a', '--adaptor', metavar='3Prime adaptor seq', default='', help='Specify the 3Prime adaptor sequence if it needs to be removed. If nothing is specified, no adaptor will be clipped form the end of the reads.')

args = options.parse_args()

#converting file names to abs paths
absFastqFile = [os.path.abspath(x) for x in args.fastqFile]

#pipeline execution
process = bioutils.seqprocess.Pipeline(args.folderName, currentFiles=absFastqFile, currentfilesType="fastq")
process.rnaSeqPipe(normalisationFactor=args.normfactor, htseqGffFile=args.gff, clip=args.adaptor)

