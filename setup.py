from distutils.core import setup


setup(name='bioutils',
      version='0.8.10',
      description='utilities for genomics and bioinformatics',
      author='Tito Candelli', 
      author_email='t.candelli@gmail.com',
      packages= ['bioutils'],
      package_data={'bioutils': ['gff/*.gff',
                                 'bowtieIndexes/*.bt2',
                                 'tools/R64+cryptic.genome',
                                 'tools/R64+cryptic.fasta',
                                 'tools/R64+cryptic.fasta.fai',
                                 'tools/R64+cryptic.chrom.sizes',
                                 'tools/pombe.genome',
                                 'tools/pombe.fasta',
                                 'tools/pombe.fasta.fai',
                                 'tools/pombe.chrom.sizes',
                                 'tools/trimmomatic-0.33.jar']},
      scripts=['scripts/cracalyze',
               'scripts/idConverter',
               'scripts/rnaseqalyze',
               'scripts/toTDF',
               'scripts/bed2coords',
               'scripts/coords2bed',
               'scripts/bedgraph2wig',
               'scripts/bed2wig',
               'scripts/sgr2wig',
               'scripts/rc',
               'scripts/wig2bedgraph',
               'scripts/wigSubtract',
               'scripts/cleaningTSSseq',
               'scripts/maskNoise',
               'scripts/afterBam',
               'scripts/maskApp']
      )



    
	
