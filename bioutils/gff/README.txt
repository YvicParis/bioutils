R64_cuts.gff3	cuts only, from xu et al 2009
R64_genes.gff3	orf-t only, from xu et al 2009
R64_other.gff3	other transcripts only, from xu et al 2009
R64_suts.gff3	suts only, from xu et al 2009
R64_tRNA.gff	tRNA only, extracted from sgd annotation R64
R64_rRNA.gff	rRNA only, extracted from sgd annotation R64
R64_cuts_suts.gff	all sgd annotation + cuts from xu et al 2009 + suts from xu et al 2009
pombe.gff3	S.pombe annotation version ASM294v2.30 downloaded from pombase