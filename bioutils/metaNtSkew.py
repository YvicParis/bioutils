import re
import bioutils
import os
import numpy
from Bio import SeqIO
import string

########################################################################
class MetaNucleotideSkew:
    """perform meta sequences analyses"""

    #----------------------------------------------------------------------
    def __init__(self, coordsFiles, fastaFile, withSeq=False, startOffset=2500, endOffset=2500):
        """Constructor"""

        #paremeters initialization
        self.startOffset = startOffset
        self.endOffset = endOffset
        self.coordsList = []
        self.wigList = []
        self.withSeq = withSeq

        #check existance of all files
        for coordsFile in coordsFiles:	
            if not os.path.exists(coordsFile):
                raise IOError("{0} not found lel.".format(coordsFile))
        absCoordsPaths = [os.path.abspath(coordsFile) for coordsFile in coordsFiles]

        if not os.path.exists(fastaFile):
            raise IOError("{0} not found lel.".format(fastaFile))
        absFastaPath = os.path.abspath(fastaFile)


        #parsing coords and wig files
        for coordsFile in absCoordsPaths:
            print "reading {}...".format(os.path.basename(coordsFile))
            if self.withSeq:    
                self.coordsList.append((bioutils.parse.CoordinatesWithSeq(coordsFile, conversion='roman'), os.path.basename(coordsFile).split(".")[0]))
            else:
                self.coordsList.append((bioutils.parse.Coordinates(coordsFile, conversion='roman'), os.path.basename(coordsFile).split(".")[0]))                
            print "done!"
            
        self.fastaFile = fastaFile


    #----------------------------------------------------------------------
    def analyze(self):
        """perform the nucleotide skew computation"""

        # save the dictionary of the fasta sequence of each chromosome in sequence
        sequence = {} 
        
        # memorize all the nucleotides sequence
        for newSeq in SeqIO.parse(self.fastaFile, "fasta"):
            # extract from the id the positions chr:start:stop
            chromo = newSeq.id
            sequence[chromo] = str(newSeq.seq)
            #print("The sequence of the chromosome {} is:\n{}".format(chromo,sequence[chromo]))

        for coordsTuple in self.coordsList:
    
            currentCoords = coordsTuple[0]
            coordsName = coordsTuple[1]
            if not os.path.exists(coordsName):
                os.makedirs(coordsName)
            
            # save the sequences extract in each position given by the coordinates
            sequences = []
            sequencesName = []
            if self.withSeq:
                for chrom, strand, start, stop, seq in currentCoords.coordsIter():
                    coord = "{}:{}:{}:{}".format(chrom, strand, start, stop)
                    sequencesName.append(coord)
                    #plus strand setting offsets
                    #find the exact coordinates according to the sequence
                    if strand == "+":
                        #chromsome checking
                        if chrom not in sequence:
                            raise TypeError('the chromosomes {0} from the fasta file ({1}) and those from the coordinates {2} do not match!\n'.format(chrom, self.fastaFile, coordsName))
                        shiftLen = [0,1,-1,2,-2,3,-3,4,-4,5,-5,6,-6,7,-7,8,-8,9,-9,10,-10,11,-11,12,-12,13,-13,14,-14,15,-15,16,-16,17,-17,18,-18,19,-19,20,-20]
                        match = 0
                        for shift in shiftLen:
                            trashSeq = sequence[chrom][ int(start)-1+shift : int(stop)+shift ]
                            if trashSeq == seq:
                                start = int(start) + shift
                                stop = int(stop) + shift
                                match += 1
                                break
                        if match != 1:
                            raise TypeError("the sequence in {0} doesn't match the sequence {1} also after a shift of {2}\n".format(coord, seq, shift))                            
                        else:
                            print("For the coord {} the shift is {}".format(coord,shift))
                            if self.startOffset is not None and self.endOffset is not None:
                                stop = int(start) + self.endOffset
                                start = int(start) - self.startOffset
                            trashseq = sequence[chrom][start-1:stop]
                            trashSmallSeq = trashseq.translate(string.maketrans("tacg", "TACG"))
                            sequences.append(trashSmallSeq)
                            print("".format(trashSmallSeq))
    
                    #minus strand setting offsets
                    if strand == "-":
                        #chromosome checking
                        if chrom not in sequence:
                            raise TypeError('the chromosomes {0} from the fasta file ({1}) and those from the coordinates {2} do not match!\n'.format(chrom, self.fastaFile, coordsName))
                        shiftLen = [0,1,-1,2,-2,3,-3,4,-4,5,-5,6,-6,7,-7,8,-8,9,-9,10,-10,11,-11,12,-12,13,-13,14,-14,15,-15,16,-16,17,-17,18,-18,19,-19,20,-20]
                        match = 0
                        for shift in shiftLen:
                            trashSeq = sequence[chrom][ int(start)-1+shift : int(stop)+shift ]
                            trashCompSeq = trashSeq.translate(string.maketrans("ATGCatgc", "TACGTACG"))
                            trashRevCompSeq = trashCompSeq[::-1]                            
                            if trashRevCompSeq == seq:
                                start = int(start) + shift
                                stop = int(stop) + shift
                                match += 1
                                break
                        if match != 1:
                            raise TypeError("the sequence in {0} doesn't match the sequence {1} also after a shift of {2}\n".format(coord, seq, shift))                            
                        else:
                            print("For the coord {} the shift is {}".format(coord,shift))                            
                            if self.startOffset is not None and self.endOffset is not None:
                                start = int(stop) - self.endOffset
                                stop = int(stop) + self.startOffset
                            trashseq = sequence[chrom][start-1:stop]
                            trashCompSeq = trashseq.translate(string.maketrans("ATGCatgc", "TACGTACG"))
                            trashRevCompSeq = trashCompSeq[::-1]                      
                            sequences.append(trashRevCompSeq)
                            print("".format(trashRevCompSeq))
                            
            else:
                for chrom, strand, start, stop in currentCoords.coordsIter():
                    coord = "{}:{}:{}:{}".format(chrom, strand, start, stop)
                    sequencesName.append(coord)
                    #plus strand setting offsets
                    if strand == "+":
                        if self.startOffset is not None and self.endOffset is not None:
                            stop = int(start) + self.endOffset
                            start = int(start) - self.startOffset
                        #chromsome checking
                        if chrom not in sequence:
                            raise TypeError('the chromosomes {0} from the fasta file ({1}) and those from the coordinates {2} do not match!\n'.format(chrom, self.fastaFile, coordsName))
                        trashseq = sequence[chrom][start-1:stop]
                        #print("start = {}\n stop = {}\n seq = {}".format(start,stop,trashseq))
                        trashSmallSeq = trashseq.translate(string.maketrans("tacg", "TACG"))
                        sequences.append(trashSmallSeq)
    
                    #minus strand setting offsets
                    if strand == "-":
                        if self.startOffset is not None and self.endOffset is not None:
                            start = int(stop) - self.endOffset
                            stop = int(stop) + self.startOffset
                        #chromosome checking
                        if chrom not in sequence:
                            raise TypeError('the chromosomes {0} from the fasta file ({1}) and those from the coordinates {2} do not match!\n'.format(chrom, self.fastaFile, coordsName))	
                        trashseq = sequence[chrom][start-1:stop]
                        #print("start = {}\n stop = {}\n seq = {}".format(start,stop,trashseq))                        
                        trashCompSeq = trashseq.translate(string.maketrans("ATGCatgc", "TACGTACG"))
                        trashRevCompSeq = trashCompSeq[::-1]
                        sequences.append(trashRevCompSeq)
                
    
            # transform the dictionary in a numpy array 2D
            nProfiles = len(sequences)
            print("In the {} coords file I have found {} sequences".format(coordsName,nProfiles))
            mgProfiles = numpy.array([list(word) for word in sequences])
            print("Thus the matrix has {0} (sequences, nucleotides long)".format(mgProfiles.shape))
            ntSkew = {}
            ntSkew['A'] = []
            ntSkew['C'] = []
            ntSkew['G'] = []
            ntSkew['T'] = []
            for i in range(self.startOffset + self.endOffset):
                uniq, occurrences = numpy.unique(mgProfiles[:,i], return_counts=True)
                trashDictio = dict(zip(uniq, occurrences))
                for nt in ntSkew:
                    if nt in trashDictio:
                        ntSkew[nt].append(float(trashDictio[nt])/nProfiles)
                    else:
                        ntSkew[nt].append(0)
                trashDictio.clear()
                        
                
            # remove the dictionary
            #mgProfiles.clear()
            #del sequences[:]
            #del sequencesName[:]
            
            
            # print the 
            outputAFile = open(coordsName + "/occurrances_A.txt", "w")
            outputAFile.write(",".join(str(x) for x in ntSkew['A']))
            outputAFile.close()
            outputCFile = open(coordsName + "/occurrances_C.txt", "w")                
            outputCFile.write(",".join(str(x) for x in ntSkew['C']))
            outputCFile.close()
            outputGFile = open(coordsName + "/occurrances_G.txt", "w")		
            outputGFile.write(",".join(str(x) for x in ntSkew['G']))
            outputGFile.close()
            outputTFile = open(coordsName + "/occurrances_T.txt", "w")                
            outputTFile.write(",".join(str(x) for x in ntSkew['T']))
            outputTFile.close()				
            
                
        # remove the dictionary
        sequence.clear()                

