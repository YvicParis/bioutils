import re
import bioutils
import os
import numpy
import cPickle
import subprocess
from time import time

from bioutils import _GFF, _TOOLS

########################################################################
class MaskSignal2Noise:
    """define the tools to perform the cleaning of the noise"""

    #----------------------------------------------------------------------
    def __init__(self, chromoLengthsFile):
        """Constructor"""
        # chromoLengthsFile is a dictionary with the lengths of each chromosome saved in .pkl format

        #paremeters initialization
        self.chromoLengths = {}

        #check existance of all files        
        if not os.path.exists(chromoLengthsFile):
            raise IOError("{0} not found lel.".format(chromoLengthsFile))
        with open(chromoLengthsFile, 'rb') as inFile:
            self.chromoLengths = cPickle.load(inFile)

    #----------------------------------------------------------------------
    def keepIfAtLeastKofN(self, plusFiles, minusFiles, minValues=2, outObject = "dictio", outputName = ""): 
        """position by position it saves the sum of the signals if it is present in at least minValues (K) samples 
        among the nReplicates (N). The default output object is a dictionary of dictionary of lists ("dictio"). 
        But one can choose also the wig object format ("wig")."""

        #paremeters initialization
        wigList = []

        #check existance of all files        
        for plusFile in plusFiles:
            if not os.path.exists(plusFile):
                raise IOError("{0} not found lel.".format(plusFile))
        absPlusPaths = [os.path.abspath(plusFile) for plusFile in plusFiles]

        for minusFile in minusFiles:
            if not os.path.exists(minusFile):
                raise IOError("{0} not found lel.".format(minusFile))
        absMinusPaths = [os.path.abspath(minusFile) for minusFile in minusFiles]

        nReplicates = len(plusFiles)
        if not len(plusFiles) == len(minusFiles):
            raise IOError("number of plus and minus wig files differs.")

        #save the wig files
        for plusFile, minusFile in zip(absPlusPaths, absMinusPaths):
            print "reading {}...".format(os.path.basename(plusFile))
            wigList.append( (bioutils.parse.Wig(plusFile), bioutils.parse.Wig(minusFile), os.path.basename(plusFile).split("_")[0]) )
            print "done!"

        #read the wig files coord by coord
        plusWigs = [wigTuple[0] for wigTuple in wigList]
        minusWigs = [wigTuple[1] for wigTuple in wigList]
        wigNames = [wigTuple[2] for wigTuple in wigList] 

        # dictionary where to save the final profiles
        self.globalProfile = {}  #dictionary of the global signal
        self.globalProfile['plus'] = {}  #dictionary of the global signal 
        globalProfileWig = {} #dictionary to construct the wig files and print them as output
        globalProfileWig['plus'] = {} # the wig object

        # the plus strand
        for chromo in plusWigs[0].chromList:
            if chromo not in self.chromoLengths:
                raise TypeError('the chromosomes from the wig {} ({}) file and those from the chromoLength list ({}) do not match!\n'.format(wigNames[0], plusWigs[0].chromList, self.chromoLengthsFile.keys()))
            self.globalProfile['plus'][chromo] = []
            self.globalProfile['plus'][chromo].append(None)
            globalProfileWig['plus'][chromo] = {}

            # be careful: since the wig is 1-based in position 0 the value is always None
            # and the last position it is saved in self.chromoLengths[chromo]
            # so the range will be (0, (self.chromoLengths[chromo] + 1) )

            valuesLists = [plusWig.rangeValue(chromo, 0, (self.chromoLengths[chromo] + 1)) for plusWig in plusWigs]
            valuesMatrix = numpy.matrix(valuesLists)

            for i in range(1,(self.chromoLengths[chromo] + 1)): 
                noneValues = 0
                sumValues = 0
                for value in numpy.array(valuesMatrix[:,i]).reshape(-1,):
                    if value is None:
                        noneValues += 1
                    else:
                        sumValues += float(value)
                if noneValues < minValues:
                    #save the sum,
                    self.globalProfile['plus'][chromo].append(sumValues)
                    globalProfileWig['plus'][chromo][i] = sumValues
                else:
                    #otherwise save none
                    self.globalProfile['plus'][chromo].append(None)

        # the minus strand
        self.globalProfile['minus'] = {}  #dictionary of the global signal            
        globalProfileWig['minus'] = {} # the wig object

        for chromo in minusWigs[0].chromList:
            if chromo not in self.chromoLengths:
                raise TypeError('the chromosomes from the wig ({}) file and those from the chromoLength list ({}) do not match!\n'.format(wigNames[0], self.chromoLengthsFile))
            self.globalProfile['minus'][chromo] = []
            self.globalProfile['minus'][chromo].append(None)
            globalProfileWig['minus'][chromo] = {}	    

            valuesLists = [minusWig.rangeValue(chromo, 0, self.chromoLengths[chromo]) for minusWig in minusWigs]
            valuesMatrix = numpy.matrix(valuesLists)

            for i in range(self.chromoLengths[chromo]):
                noneValues = 0
                sumValues = 0
                for value in numpy.array(valuesMatrix[:,i]).reshape(-1,):
                    if value is None:
                        noneValues += 1
                    else:
                        sumValues += float(value)
                if noneValues < minValues:
                    #save the sum,
                    self.globalProfile['minus'][chromo].append(sumValues)
                    globalProfileWig['minus'][chromo][i] = sumValues		    
                else:
                    #otherwise save none
                    self.globalProfile['minus'][chromo].append(None)

        # print the global profiles to wig
        # in order to do that I must convert the lists into dictionaries
        if outputName is "":    
            baseName = wigNames[0].split("_")[0][:-1]
        else:
            baseName = outputName

        globalProfileWig['plus']['name'] = baseName + "_plus"
        plusWig = bioutils.parse.Wig(globalProfileWig['plus'])
        plusOutputFileName = baseName + "_globalProfile_plus.wig"
        plusWig.writeWig(plusOutputFileName)

        globalProfileWig['minus']['name'] = baseName + "_minus"
        minusWig = bioutils.parse.Wig(globalProfileWig['minus'])
        minusOutputFileName = baseName + "_globalProfile_minus.wig"
        minusWig.writeWig(minusOutputFileName)	

        if outObject == "dictio":                   
            return self.globalProfile
        elif outObject == "wig":
            return globalProfileWig
        else: 
            raise IOError("ERROR!! The outObject option {0} is not valid. Select one between 'dictio' and 'wig'.".format(outObject))	    

    #----------------------------------------------------------------------
    def Clustering(self, globalProfile, nameClustering, clusterDistance=50): 
        """position by position it clusterize the value that are nearer than clusterDistance."""

        # define the dictionary where to save the positions and value of the cluster
        clusterCoords = {}


        #read the globalProfile wig files coord by coord
        for strand in ['plus','minus']:
            clusterCoords[strand] = []
            strandSymbol = '+' if strand == 'plus' else '-'
            for chromo in globalProfile[strand]:
                # initialiaze the counters for each chromosome
                sumValues = 0
                dist = 0
                tmpStop = 0
                for i, value in enumerate(globalProfile[strand][chromo]):
                    # cicle on the position along the chromosome
                    if value is None:
                        dist += 1
                    else: 
                        # the value in position i is defined
                        if dist < clusterDistance:
                            # initializing start for the first cluster
                            if tmpStop == 0:
                                start = i
                            # the distance between the last two signals is smaller than the threshold
                            # sum the value in the same cluster
                            sumValues += float(value)
                            dist = 0
                            tmpStop = i
                        else:
                            # the distance between the last two signals is greater(equal) than the threshold
                            if tmpStop != 0:
                                # save the cluster only if the cluster is already defined
                                stop = tmpStop
                                # save the cluster
                                clusterCoords[strand].append("{0}:{1}:{2}:{3}:{4}:{5}".format(chromo,strandSymbol,start,stop,sumValues,nameClustering) ) #TODO: understand the right order between sumValues and nameClustering
                            # reinitialize the counters
                            dist = 0 
                            sumValues = float(value)
                            start = i
                            tmpStop = i

                # save the last cluster
                if tmpStop != 0:
                    # save the cluster only if the cluster is already defined
                    stop = tmpStop
                    # save the cluster
                    clusterCoords[strand].append("{0}:{1}:{2}:{3}:{4}:{5}".format(chromo,strandSymbol,start,stop,sumValues,nameClustering) ) #TODO: understand the right order between sumValues and nameClustering


        # TODO: how to deal with the last cluster 
        # (because in this implementation the saving process it is performed when I found the new start, 
        # but for the last cluster there will not be a new start after the last stop,
        # I need something that test something for the last position tested...still thinking about it....)

        return clusterCoords

    #----------------------------------------------------------------------
    def NormOnPombeClustering(self, bamFiles, clustering, normalisationFactor): 
        """measure the total number of reads that map on pombe for all the bam files given as input. 
        Divide the clustering values by the total number of reads that map on pombe."""

        # define the dictionary where to save the normalized value of the cluster
        normalizedClusterCoords = {}
        normClusArray = {}

        nTotalMappedReads = 0
        # measure for each file the total number of reads that map on pombe
        for bamFile in bamFiles:
            if not os.path.exists(bamFile):
                raise IOError("Bam file {0} not found.".format(bamFile))
            if bamFile[-3:] != "bam":
                raise TypeError("the file must be a bam file. There was a problem with {}.".format(bamFile))            
            filePath = os.path.abspath(bamFile)
            sampleMappedSize = subprocess.check_output(['samtools','view','-c','-F','4','{0}'.format(filePath)])
            nTotalMappedReads += int(sampleMappedSize)
        # define the normalization coefficient
        normCoeff = float(normalisationFactor) / nTotalMappedReads

        # plus strand
        normalizedClusterCoords['plus'] = []
        normClusArray['plus'] = {}        
        clusteringPlusData = bioutils.parse.CoordinatesWithName(clustering['plus'])        
        for chromo, strand, start, stop, sumValues, nameClustering in clusteringPlusData.coordsIter():
            normalizedSumValues = float(sumValues) * normCoeff
            coord = "{}:{}:{}:{}:{}:{}".format(chromo, strand, start, stop, normalizedSumValues, nameClustering) 
            normalizedClusterCoords['plus'].append(coord)
            if chromo not in normClusArray['plus']:
                normClusArray['plus'][chromo] = numpy.zeros((self.chromoLengths[chromo] + 1))
            newValue = normalizedSumValues / (int(stop) - int(start) + 1)
            for pos in range(int(start),(int(stop)+1)):
                normClusArray['plus'][chromo][pos] = newValue

        # minus strand
        normalizedClusterCoords['minus'] = []
        normClusArray['minus'] = {}                
        clusteringMinusData = bioutils.parse.CoordinatesWithName(clustering['minus'])        
        for chromo, strand, start, stop, sumValues, nameClustering in clusteringMinusData.coordsIter():
            normalizedSumValues = float(sumValues) * normCoeff
            coord = "{}:{}:{}:{}:{}:{}".format(chromo, strand, start, stop, normalizedSumValues, nameClustering) 
            normalizedClusterCoords['minus'].append(coord)
            if chromo not in normClusArray['minus']:
                normClusArray['minus'][chromo] = numpy.zeros((self.chromoLengths[chromo] + 1))
            newValue = normalizedSumValues / (int(stop) - int(start) + 1)
            for pos in range(int(start),(int(stop)+1)):
                normClusArray['minus'][chromo][pos] = newValue

        print "in norm: final"
        print clustering['plus'][1:5]
        print clustering['minus'][30:32]


        return (normalizedClusterCoords, normClusArray)       

    #----------------------------------------------------------------------
    def MaskDefinition(self, signalNormalizedClusterCoords, signalNormClusArray, noiseNormalizedClusterCoords, noiseNormClusArray): 
        """position by position define in a dictionary of arrays if the TSS-seq signals of that position must be taken or removed.
        Keep if mask==1, remove if mask==0."""

        mask = {}

        # The analysis is driven by the signal
        mask['plus'] = {}
        sigNormClustPlusData = bioutils.parse.CoordinatesWithName(signalNormalizedClusterCoords['plus'])        
        for chromo, strand, start, stop, signalValue, nameClustering in sigNormClustPlusData.coordsIter():
            # sum the values in the noise sample
            noiseValue = sum( noiseNormClusArray['plus'][chromo][int(start):(int(stop)+1)] )
            # define the mask as a dictionary of arrays full of value 1 (1 means save, 0 means remove).
            if chromo not in mask['plus']:
                mask['plus'][chromo] = numpy.ones(self.chromoLengths[chromo] + 1)
            # if the noise signal is equal 0 keep the region (save 1) 
            if noiseValue != 0:
                ratioR = float(signalValue) / noiseValue
                # if the noise signal is different from 0 and the ratio is smaller than 1, remove the region (save 0) 
                if ratioR <= 1:
                    for pos in range(int(start),(int(stop)+1)):
                        mask['plus'][chromo][pos] = 0

        mask['minus'] = {}
        sigNormClustMinusData = bioutils.parse.CoordinatesWithName(signalNormalizedClusterCoords['minus'])        
        for chromo, strand, start, stop, signalValue, nameClustering in sigNormClustMinusData.coordsIter():
            # sum the values in the noise sample
            noiseValue = sum( noiseNormClusArray['minus'][chromo][int(start):(int(stop)+1)] )
            # define the mask as a dictionary of arrays full of value 1 (1 means save, 0 means remove).
            if chromo not in mask['minus']:
                mask['minus'][chromo] = numpy.ones(self.chromoLengths[chromo] + 1)
            # if the noise signal is equal 0 keep the region (save 1) 
            if noiseValue != 0:
                ratioR = float(signalValue) / noiseValue
                # if the noise signal is different from 0 and the ratio is smaller than 1, remove the region (save 0) 
                if ratioR <= 1:
                    for pos in range(int(start),(int(stop)+1)):
                        mask['minus'][chromo][pos] = 0

        # save mask in .pkl format
        with open("mask.pkl",'wb') as dictio:
            cPickle.dump(mask, dictio, -1)        

        # save mask in .coords format
        maskCoordsList = []
        for strand in ['plus','minus']:
            strandSymbol = '+' if strand == 'plus' else '-'
            for chromo in mask[strand]:
                trashValue = 0
                for pos, value in enumerate(mask[strand][chromo]):
                    # never save the coords 0, since it doesn't exist
                    if pos == 0:
                        value = 0
                    # value can be only 0 or 1
                    # if value > trashValue it means that value(pos-1)=0 and value(pos)=1 --> pos is a start point
                    if int(value) > trashValue:
                        start = int(pos)
                    # if value < trashValue it means that value(pos-1)=1 and value(pos)=0 --> pos is a stop point
                    elif int(value) < trashValue:
                        stop = int(pos - 1)
                        # save the coords
                        coords = "{}:{}:{}:{}:1:keepValue".format(chromo, strandSymbol, start, stop)
                        #print coords
                        maskCoordsList.append(coords)
                    else:
                        continue
                    trashValue = int(value)
        maskCoords = bioutils.parse.CoordinatesWithName(maskCoordsList)
        # I will print the coords file
        maskCoords.writeCoords(outFile="mask.coords")
        # now I need to print it by hand
        #outputFileNames = "mask.coords"
        #outFile = open(outputFileNames,'w')
        #for line in maskCoordsList:			
        #    outFile.write("{}\n".format(line))
        #outFile.close()
        # when the bedFormat function for the class CoordinatesWithName in the parse.py script will be correct
        # I will print the bed file also
        maskCoords.bedFormat(outFile="mask.bed")   


        return mask

    #----------------------------------------------------------------------
    def MaskApplication(self, mask, plusFiles, minusFiles, outNames=[]): 
        """function that applies the filtering mask to eliminate the noise from a TSS-seq wig sample. Keep if mask==1, remove if mask==0."""

        # dictionary where to save the final filtered profiles
        self.maskProfile = {}  

        #paremeters initialization
        wigList = []

        # check if the mask is a dictionary or a file
        if type(mask) is dict:
            self.mask = mask  

        elif type(mask) is str and os.path.exists(mask):
            if not os.path.isfile(mask):
                raise IOError('could not read file {}'.format(mask))
            if mask[-6:] != "coords":
                raise TypeError("the file must be a .coords file. There was a problem with {}.".format(mask)) 	    
            #read the maskFile.coords and save it in the self.mask dictionary
            self.mask = {}
            self.mask['plus'] = {}
            self.mask['minus'] = {}
            maskCoords = bioutils.parse.CoordinatesWithName(mask)        
            for chromo, strandSymbol, start, stop, keepValue, action in maskCoords.coordsIter():
                # define the self.mask as a dictionary of arrays full of value 1 (1 means save, 0 means remove).
                strand = 'plus' if strandSymbol == '+' else 'minus'
                if chromo not in self.mask[strand]:
                    self.mask[strand][chromo] = numpy.zeros(self.chromoLengths[chromo] + 1)
                for pos in range(int(start),int(stop)+1):
                    self.mask[strand][chromo][pos] = int(keepValue)

        else:
            raise TypeError("The input mask must be either a dictionary of values, a file name or a path to a file that exists. There was a problem with {}.".format(mask))


        # save the absolute path to the wig plus
        for plusFile in plusFiles:
            if not os.path.exists(plusFile):
                raise IOError("{0} not found lel.".format(plusFile))
        absPlusPaths = [os.path.abspath(plusFile) for plusFile in plusFiles]

        # save the absolute path to the wig minus
        for minusFile in minusFiles:
            if not os.path.exists(minusFile):
                raise IOError("{0} not found lel.".format(minusFile))
        absMinusPaths = [os.path.abspath(minusFile) for minusFile in minusFiles]

        # select the output names and open the wig files
        if outNames:
            for plusFile, minusFile, outName in zip(absPlusPaths, absMinusPaths, outNames):
                print "reading {}...".format(os.path.basename(plusFile))
                wigList.append( (bioutils.parse.Wig(plusFile), bioutils.parse.Wig(minusFile), os.path.basename(plusFile).split("_")[0], outName) )
                print "done!"
        else:
            for plusFile, minusFile in zip(absPlusPaths, absMinusPaths):
                wigName = os.path.basename(plusFile).split("_")[0]
                outName = os.path.basename(wigName).split(".")[0].split("_")[0][:-1] + "_afterMask"
                print "reading {}...".format(os.path.basename(plusFile))
                wigList.append( (bioutils.parse.Wig(plusFile), bioutils.parse.Wig(minusFile), os.path.basename(plusFile).split("_")[0], outName) )
                print "done!"


        for wigTuple in wigList:
            plusWig = wigTuple[0]
            minusWig = wigTuple[1]
            wigName = wigTuple[2]
            outName = wigTuple[3]

            self.maskProfile[wigName] = {}
            self.maskProfile[wigName]['plus'] = {}
            self.maskProfile[wigName]['minus'] = {}

            # parse the wig chromosome by chromosome 
            # defining the numpy arrays of the value and the mask
            # and evaluating the product between the two arrays
            for chromo in self.mask['plus']:
                # the wig object are defined as dictionaries because they are sparse arrays
                self.maskProfile[wigName]['plus'][chromo] = {}
                self.maskProfile[wigName]['minus'][chromo] = {}                
                # extract the list of values
                plusValuesList = plusWig.rangeValue(chromo, 0, (self.chromoLengths[chromo])) 
                minusValuesList = minusWig.rangeValue(chromo, 0, (self.chromoLengths[chromo])) 
                # transform the lists in arrays
                plusValuesArray = numpy.array(plusValuesList, dtype= float)
                minusValuesArray = numpy.array(minusValuesList, dtype= float)
                # compute the filtered values
                plusFiltValArr = plusValuesArray * self.mask['plus'][chromo]
                minusFiltValArr = minusValuesArray * self.mask['minus'][chromo]
                # save the self.maskProfile only when the value is defined and different from 0
                for pos, value in enumerate(plusFiltValArr):
                    if numpy.isnan(value) or value == 0:
                        continue
                    else:
                        self.maskProfile[wigName]['plus'][chromo][pos] = float(value)
                for pos, value in enumerate(minusFiltValArr):
                    if numpy.isnan(value) or value == 0:
                        continue
                    else:
                        self.maskProfile[wigName]['minus'][chromo][pos] = float(value)


            # transform the mask profile in a wig object, in order to be able to print it as 2 wig files.
            wigObj = self.maskProfile[wigName]['plus']
            wigObj['name'] = wigName
            wigPlus = bioutils.parse.Wig(wigObj)
            wigPlus.writeWig(outName + "_plus.wig")

            wigObj = self.maskProfile[wigName]['minus']
            wigObj['name'] = wigName
            wigMinus = bioutils.parse.Wig(wigObj)
            wigMinus.writeWig(outName + "_minus.wig")


        # return the dictionary containing all the wig objects    
        return self.maskProfile


    #----------------------------------------------------------------------
    def maskPipe(self, signalPlusWig, signalMinusWig, noisePlusWig, noiseMinusWig, signalOnPombeBam, noiseOnPombeBam, normalisationFactor=10000000):      
        """execute all appropriate methods to perform the creation of the mask to filter out the noise."""

        timeStart= time()

        # filtering the signal samples
        signalGP = self.keepIfAtLeastKofN(signalPlusWig, signalMinusWig)
        signalCC = self.Clustering(signalGP, 'signal')
        print "pre-norm"
        print signalCC['plus'][1:5]
        print signalCC['minus'][30:32]       
        (signalNCC, signalNCA) = self.NormOnPombeClustering(signalOnPombeBam, signalCC, normalisationFactor/10)
        print "post-norm"
        print signalNCC['plus'][1:5]
        print signalNCC['minus'][30:32]         


        deltaTime = (time() - timeStart)/60
        print("Signal filtering: {0:.2f} minutes.".format(deltaTime))
        timeStart= time()		

        # filtering the noise samples
        noiseGP = self.keepIfAtLeastKofN(noisePlusWig, noiseMinusWig)
        noiseCC = self.Clustering(noiseGP, 'noise')
        (noiseNCC, noiseNCA) = self.NormOnPombeClustering(noiseOnPombeBam, noiseCC, normalisationFactor/10)

        deltaTime = (time() - timeStart)/60
        print("Noise filtering: {0:.2f} minutes.".format(deltaTime))
        timeStart= time()		

        # define the mask
        mask = self.MaskDefinition(signalNCC,signalNCA, noiseNCC, noiseNCA) 

        deltaTime = (time() - timeStart)/60
        print("Mask definition: {0:.2f} minutes.".format(deltaTime))
        timeStart= time()		


    #----------------------------------------------------------------------
    def NormOnPombeGlobalProfile(self, globalProfile, bamFiles, outName, genomeBaseline="pombe", genomeSignal="R64+cryptic", normalisationFactor=10000000, minLength=40):  #TODO:WRITE STATLOG METHOD FOR NORMWIG, REPORT SIZE FACTORS.
        """Launch the mapping on pombe for the reads longer than minLength that do not map on cerevisiae.
        Measure the total number of reads that map on pombe for all the bam files given as input. And sum them up.
        Divide the values of the given wig dictionary by the total number of reads that map on pombe (multiply by the normalization factor).
        NB: the globalProfile given as input is a wig dictionary of wig objects"""        

        # Function divided in two part:
        # 1.measuring the total number of reads that map on pombe for all the replicates and sum them up.
        # 2.normalizing the globalProfile

        # First part
        print "Normalizing on {} process..".format(genomeBaseline)

        nTotalMappedReads = 0
        nReplicates = 0
        # measure for each file the total number of reads that map on pombe
        for bamFile in bamFiles:
            if not os.path.exists(bamFile):
                raise IOError("Bam file {0} not found.".format(bamFile))
            if bamFile[-3:] != "bam":
                raise TypeError("the file must be a bam file. There was a problem with {}.".format(bamFile))
            nReplicates += 1
            bamfilePath = os.path.abspath(bamFile)
            bamFileName = os.path.basename(bamFile)
            # define a bioutils.seqprocess.Pipeline for this replicate
            currDir = os.getcwd()
            print "defining the {} class.".format(nReplicates)
            aReplicate = bioutils.seqprocess.Pipeline(currDir + "/mapOnPombe_" + outName + "_" + str(nReplicates))

            #generate the fasta file of reads that do not map on cerevisiae and that are longer than minLength bases
            currentFilesNames = [bamfilePath]								
            aReplicate.metadata.storeOutput('pre_sieve', currentFilesNames, 'bam')
            aReplicate.sieve(minLength)				

            #check correct input format
            inputFilePath = aReplicate.metadata.processedFileNames['sieve']
            fileTypes = [x.split(".")[-1] for x in inputFilePath]
            if fileTypes[0] != 'fasta' and fileTypes[0] != 'fastq':
                raise IOError('incorrect filetype for mapping, this operation requires fasta or fastq input files.\n')

            trashName = bamFileName.split(".")[0]
            shortName = trashName.split("_")[0]

            #initialize the aReplicate.metadata.currentFilesNames with the inputFilePath
            currentFilesNames = inputFilePath
            aReplicate.metadata.storeOutput('pre_map_{}'.format(genomeBaseline), 
                                            currentFilesNames, 
                                            fileTypes[0])					

            #perform the mapping over pombe
            aReplicate.bowtieMap(genome=genomeBaseline, seedMismatches=0, threads=8)

            #and transform the sam in bam files
            aReplicate.bam()
            newBamFilePaths = aReplicate.metadata.currentFilesNames
            #after this calling in aReplicate.metadata the files labeled with 'bam' are the ones mapped on pombe

            for filePath in newBamFilePaths:
                print("measuring the number of reads mapping on pombe in the sample {}...".format(shortName))                
                sampleMappedSize = subprocess.check_output(['samtools','view','-c','-F','4','{0}'.format(filePath)])
                print("In sample {0} there are {1} reads of pombe.".format(shortName, sampleMappedSize))
                nTotalMappedReads += int(sampleMappedSize)
        # define the normalization coefficient
        normCoeff = float(normalisationFactor) / nTotalMappedReads
        print("{}".format(normCoeff))

        # Second part

        # getting names and output files
        plusOutputFileName = outName + "_normalizedOnPombe_plus.wig" 
        minusOutputFileName = outName + "_normalizedOnPombe_minus.wig"
        outputFilesNamesList = [plusOutputFileName] + [minusOutputFileName]				
        # executing ..
        print("Generating normalized on pombe Wigs...")
        # Generating the new wig file from the input dictionary
        plusWig = bioutils.parse.Wig(globalProfile['plus'])
        plusWig.alphaWig(normCoeff)
        plusWig.writeWig(plusOutputFileName)
        minusWig = bioutils.parse.Wig(globalProfile['minus'])
        minusWig.alphaWig(normCoeff)
        minusWig.writeWig(minusOutputFileName)
        print("Done!")

        # TDF conversion
        print("Generating TDFs, this might take a while...")
        #print("creating TDF folder...")
        if not os.path.exists("TDF"):
            os.mkdir("TDF")
        #print("converting toTDF...")
        for fileName in [fileName[:-4] for fileName in outputFilesNamesList]:
            query = "igvtools toTDF {0}.wig TDF/{0}.tdf {1}{2}.genome".format(fileName, _TOOLS, genomeSignal)
            print("""executing:\n"""+query)
            toTDFOutput = subprocess.check_output(query, shell=True, stderr=subprocess.STDOUT)
            #print("conversion from wig to tdf format: {}".format(toTDFOutput))
        #print("files moved.")				
        print("Done!")


        print("Generating bigWigs, this might take a while...")
        # bigWig conversion
        #print("creating BigWig folder...")
        if not os.path.exists("BigWig"):
            os.mkdir("BigWig")
        # print the wig without the 2-micron chromosome
        #print("eliminating the 2-micron chromosome...")
        for fileName in [fileName[:-4] for fileName in outputFilesNamesList]:
            query = '''cat {0}.wig | awk '/^variableStep/{{split($2,a,"=");chromo=a[2]}} {{if(chromo!="2-micron")print($0);}}' > {0}_no2micron.wig'''.format(fileName)
            print("""executing:\n"""+query)
            toRm2micronOutput = subprocess.check_output(query, shell=True, stderr=subprocess.STDOUT)
            #print("wig without 2-micron chromosome: {}".format(toRm2micronOutput))				
        # convert the wig into bigWigs
        print("converting to BigWig...")
        for fileName in [fileName[:-4] for fileName in outputFilesNamesList]:
            query = "wigToBigWig {0}_no2micron.wig {1}{2}.chrom.sizes BigWig/{0}.bw".format(fileName, _TOOLS, genomeSignal)
            print("""executing:\n"""+query)
            toBigWigOutput = subprocess.check_output(query, shell=True, stderr=subprocess.STDOUT)
            #print("conversion from wig to BigWig format: {}".format(toBigWigOutput))	
        # delete the auxiliar wigs
        #print("removing auxiliar wigs...")
        for fileName in [fileName[:-4] for fileName in outputFilesNamesList]:
            query = "rm {0}_no2micron.wig".format(fileName)
            #print("""executing:\n"""+query)
            toRMOutput = subprocess.check_output(query, shell=True, stderr=subprocess.STDOUT)
            #print("delete auxiliar wigs: {}".format(toRMOutput))
        #print("files moved.")				



    #----------------------------------------------------------------------
    def cleaningPipe(self, plusFiles, minusFiles,  
                     bamFiles, outName = "", minValues=2, genomeBaseline="pombe", 
                     genomeSignal="R64+cryptic", normalisationFactor=10000000, minLength=40):      
        """take as input the replicates, keep only signal that is present in K replicates out of N,
        from this new wig generate the normalized on pombe wig 
        (as input needs the bamFiles of the mapping on cerevisiae of the replicates)"""

        if not outName:
            outName = os.path.basename(plusFiles[0]).split(".")[0].split("_")[0][:-1]	

        timeStart= time()

        # filtering the signal samples
        globalProfile = self.keepIfAtLeastKofN(plusFiles, minusFiles, minValues, "wig", outName)

        deltaTime = (time() - timeStart)/60
        print("Cleaning (generating global profile): {0:.2f} minutes.".format(deltaTime))
        timeStart= time()

        self.NormOnPombeGlobalProfile(globalProfile, bamFiles, outName, normalisationFactor=normalisationFactor/10)

        deltaTime = (time() - timeStart)/60
        print("Normalizing on pombe: {0:.2f} minutes.".format(deltaTime))
        timeStart= time()		

