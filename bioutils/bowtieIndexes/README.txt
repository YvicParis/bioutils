pombe	S.pombe whole genome version ASM294v2.30 downloaded from pombase
R64_tRNA	cerevisiae tRNA genes, each gene is a chromosome
R64_rRNA	cerevisiae rRNA genes, each gene is a chromosome
R64	cerevisiae whole genome
R64mutant	cerevisiae whole genome with mutations introduced by Drice in the introns of RPL7B and RPL28