import os
__all__=['seqprocess','misc','metagene','parse','metaNtSkew','maskSignal2Noise']
_ROOT = os.path.abspath(os.path.dirname(__file__))
_GFF = _ROOT + "/gff/"
_INDEXES = _ROOT + "/bowtieIndexes/"
_TOOLS = _ROOT + "/tools/"

os.environ["BOWTIE2_INDEXES"] = _INDEXES


import bioutils.misc
import bioutils.parse
import bioutils.seqprocess
import bioutils.metagene
import bioutils.metaNtSkew
import bioutils.maskSignal2Noise