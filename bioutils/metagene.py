import re
import bioutils
import os
import numpy

########################################################################
class Metagene:
        """perform metagene/metasite analyses"""

        #----------------------------------------------------------------------
        def __init__(self, coordsFiles, plusFiles, minusFiles, startOffset=2500, endOffset=2500):
                """Constructor"""

                #paremeters initialization
                self.startOffset = startOffset
                self.endOffset = endOffset
                self.coordsList = []
                self.wigList = []

                #check existance of all files
                for coordsFile in coordsFiles:	
                        if not os.path.exists(coordsFile):
                                raise IOError("{0} not found lel.".format(coordsFile))
                absCoordsPaths = [os.path.abspath(coordsFile) for coordsFile in coordsFiles]

                for plusFile in plusFiles:
                        if not os.path.exists(plusFile):
                                raise IOError("{0} not found lel.".format(plusFile))
                absPlusPaths = [os.path.abspath(plusFile) for plusFile in plusFiles]

                for minusFile in minusFiles:
                        if not os.path.exists(minusFile):
                                raise IOError("{0} not found lel.".format(minusFile))
                absMinusPaths = [os.path.abspath(minusFile) for minusFile in minusFiles]

                if not len(plusFiles) == len(minusFiles):
                        raise IOError("number of plus and minus wig files differs.")

                #parsing coords and wig files
                for coordsFile in absCoordsPaths:
                        print "reading {}...".format(os.path.basename(coordsFile))
                        self.coordsList.append((bioutils.parse.Coordinates(coordsFile, conversion='roman'), os.path.basename(coordsFile).split(".")[0]))
                        print "done!"

                for plusFile, minusFile in zip(absPlusPaths, absMinusPaths):
                        print "reading {}...".format(os.path.basename(plusFile))
                        self.wigList.append( (bioutils.parse.Wig(plusFile), bioutils.parse.Wig(minusFile), os.path.basename(plusFile)[:-4]) )
                        print "done!"



        #----------------------------------------------------------------------
        def analyze(self, function='median', rmOutliers=False, oneValue=False, printProfiles=True):
                """perform the actual number crunching"""

                if function not in ['mean', 'median', 'std', "sum"]:
                        raise TypeError("function not recognized.\n")

                for coordsTuple in self.coordsList:

                        currentCoords = coordsTuple[0]
                        coordsName = coordsTuple[1]
                        if not os.path.exists(coordsName):
                                os.makedirs(coordsName)

                        for wigTuple in self.wigList:


                                plusWig = wigTuple[0]
                                minusWig = wigTuple[1]
                                wigName = wigTuple[2]

                                #finalDistr = dict((i, []) for i in range(self.startOffset+self.endOffset+1)) 
                                mgProfile = {}  #dictionary that associates the coordinates 4-tupla with the corresponding normalized coverage vector, aligned in the correct direction


                                for chrom, strand, start, stop in currentCoords.coordsIter():
                                        coord = "{}:{}:{}:{}".format(chrom, strand, start, stop)
                                        #setting offsets
                                        if strand == "+":
                                                if self.startOffset is not None and self.endOffset is not None:
                                                        stop = int(start) + self.endOffset
                                                        start = int(start) - self.startOffset
                                                #chromosome checking
                                                if chrom not in plusWig.chromList:
                                                        raise TypeError('the chromosomes from the wig ({}) file and those from the coordinates ({}) do not match!\n'.format(wigName, coordsName))
                                                mgProfile[coord]=numpy.zeros(stop-start+1)

                                                valuesList = plusWig.rangeValue(chrom, start, stop)
                                                for i, value in enumerate(valuesList):
                                                        if value is None:
                                                                continue
                                                        if oneValue:
                                                                #finalDistr[i].append(1)
                                                                mgProfile[coord][i] = 1
                                                        else:
                                                                #finalDistr[i].append(value)
                                                                mgProfile[coord][i] = value
                                        #minus strand setting offsets
                                        if strand == "-":
                                                if self.startOffset is not None and self.endOffset is not None:
                                                        start = int(stop) - self.endOffset
                                                        stop = int(stop) + self.startOffset
                                                        #chromosome checking
                                                if chrom not in minusWig.chromList:
                                                        raise TypeError('the chromosomes from the wig file and those from the coordinates do not match!\n')					
                                                mgProfile[coord]=numpy.zeros(stop-start+1)

                                                valuesList = minusWig.rangeValue(chrom, start, stop)
                                                valuesList = valuesList[::-1]
                                                for i, value in enumerate(valuesList):
                                                        if value is None:
                                                                continue
                                                        if oneValue:
                                                                #finalDistr[i].append(1)
                                                                mgProfile[coord][i] = 1
                                                        else:
                                                                #finalDistr[i].append(value)
                                                                mgProfile[coord][i] = value

                                #keys = finalDistr.keys()
                                #keys.sort()

                                # transform the dictionary in a numpy array 2D
                                keys = mgProfile.keys()
                                keys.sort()
                                nProfiles = len(keys)
                                mgProfiles = numpy.zeros(shape=(nProfiles,self.startOffset+self.endOffset+1))
                                for i,key in enumerate(keys):
                                        mgProfiles[i] = mgProfile[key]
                                        del mgProfile[key]
                                # remove the mgProfile dictionary
                                mgProfile.clear()

                                #the outliers value are transformed in negative value, then in the calculation of the mean only the values greater than 0 are taken into account
                                if rmOutliers:
                                        for i in range(self.startOffset+self.endOffset+1):
                                        #for key in keys:
                                                #if len(finalDistr[key]) == 0:
                                                if numpy.sum(mgProfiles[:,i]) == 0:
                                                        continue
                                                else:
                                                        mean = numpy.mean([x for x in mgProfiles[:,i] if x != 0])
                                                        sd = numpy.std([x for x in mgProfiles[:,i] if x != 0])
                                                        mgProfiles[:,i] = [x if x <= float(mean) + 5 * float(sd) else -x for x in mgProfiles[:,i]]
                                                        #mean = numpy.mean(finalDistr[key])
                                                        #sd = numpy.std(finalDistr[key])                            
                                                        #finalDistr[key] = [x for x in finalDistr[key] if x <= float(mean) + 5 * float(sd)]                                                        
                                        #print the outliers profiles (#TODO)

                                if printProfiles:
                                        outputProfiles = open(coordsName+"/"+wigName+"_profiles.csv", "w")
                                        for i,key in enumerate(keys):
                                                trash = key.split(":")
                                                outputProfiles.write(",".join(str(x) for x in trash) + "," + ",".join(str(x) for x in mgProfiles[i]) + "\n")


                                outputFile = open(coordsName+"/"+wigName+".txt", "w")
                                meanVector = numpy.zeros(self.startOffset+self.endOffset+1)

                                if function == 'median':
                                        #meanVector = []                    
                                        #for key in keys:
                                                #meanVector.append(numpy.median(finalDistr[key]))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        for i in range(len(meanVector)):
                                                meanVector[i] = numpy.median([x for x in mgProfiles[:,i] if x > 0])
                                        outputFile.write(",".join(str(x) for x in meanVector))
                                        outputFile.close()
                                elif function == 'mean':
#                    meanVector = []
#                    for key in keys:
#                        meanVector.append(numpy.mean(finalDistr[key]))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        for i in range(len(meanVector)):                    
                                                meanVector[i] = numpy.mean([x for x in mgProfiles[:,i] if x > 0])
                                        outputFile.write(",".join(str(x) for x in meanVector))
                                        outputFile.close()
                                elif function == 'std':
#                    meanVector = []
#                    for key in keys:
#                        meanVector.append(numpy.std(finalDistr[key]))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        for i in range(len(meanVector)):                   
                                                meanVector[i] = numpy.std([x for x in mgProfiles[:,i] if x > 0])
                                        outputFile.write(",".join(str(x) for x in meanVector))
                                        outputFile.close()
                                elif function == 'sum':
#                    meanVector = []
#                    for key in keys:
#                        meanVector.append(numpy.sum(finalDistr[key]))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        for i in range(len(meanVector)):                   
                                                meanVector[i] = numpy.sum([x for x in mgProfiles[:,i] if x > 0])
                                        outputFile.write(",".join(str(x) for x in meanVector))
                                        outputFile.close()				


        #----------------------------------------------------------------------
        def interval(self, function='median', rmOutliers=False, oneValue=False, printProfiles=True, minValues=5, align="begin"):
                """perform the actual number crunching. the interval method performs the analysis with intervals and
                not fixed windows around a point. when intervals of different lengths are provided, these are aligned
                at the start and aggregated up until their end, not further."""

                if function not in ['mean', 'median', 'std', "sum"]:
                        raise TypeError("function not recognized.\n")

                if align not in ['begin', 'end']:
                        raise TypeError("align set {} not recognized. Choose align value between 'begin' and 'end' of the interval\n".format(align))


                for coordsTuple in self.coordsList:

                        currentCoords = coordsTuple[0]
                        coordsName = coordsTuple[1]
                        if not os.path.exists(coordsName):
                                os.makedirs(coordsName)

                        for wigTuple in self.wigList:


                                plusWig = wigTuple[0]
                                minusWig = wigTuple[1]
                                wigName = wigTuple[2]

                                #finalDistr =  [] #dict((i, []) for i in range(self.startOffset+self.endOffset+1))  #
                                mgProfile = {}  #dictionary that associates the coordinates 4-tupla with the corresponding normalized coverage vector, aligned in the correct direction
                                lenProfile = [] #list of the length of the profiles

                                for chrom, strand, start, stop in currentCoords.coordsIter():
                                        coord = "{}:{}:{}:{}".format(chrom, strand, start, stop)
                                        lenProfile.append(int(stop)-int(start)+1)
                                        #setting offsets
                                        if strand == "+":
                                                #chromosome checking
                                                if chrom not in plusWig.chromList:
                                                        raise TypeError('the chromosomes from the wig ({}) file and those from the coordinates ({}) do not match!\n'.format(wigName, coordsName))

                                                mgProfile[coord]=numpy.empty(int(stop)-int(start)+1) * numpy.nan #initialize all the vector to nan values
                                                valuesList = plusWig.rangeValue(chrom, start, stop)
                                                if oneValue:
                                                        for i, value in enumerate(valuesList):
                                                                if value is None:
                                                                        continue
                                                                else:
                                                                        mgProfile[coord][i] = 1                                                                
                                                else:
                                                        mgProfile[coord] = numpy.array(valuesList, dtype=numpy.float)
                                                        #if len(finalDistr) == i and value is None:
                                                                #finalDistr.append([])
                                                        #elif len(finalDistr) == i and value is not None:
                                                                #finalDistr.append([value])
                                                        #elif value is None:
                                                                #continue
                                                        #else:
                                                                #finalDistr[i].append(value)

						

                                        #minus strand setting offsets
                                        if strand == "-":
                                                #chromosome checking
                                                if chrom not in minusWig.chromList:
                                                        raise TypeError('the chromosomes from the wig file and those from the coordinates do not match!\n')

                                                mgProfile[coord]=numpy.empty(int(stop)-int(start)+1) * numpy.nan #initialize all the vector to nan values
                                                valuesList = minusWig.rangeValue(chrom, start, stop)
                                                valuesList = valuesList[::-1]
                                                if oneValue:
                                                        for i, value in enumerate(valuesList):
                                                                if value is None:
                                                                        continue
                                                                else:
                                                                        mgProfile[coord][i] = 1                                                                
                                                else:
                                                        mgProfile[coord] = numpy.array(valuesList, dtype=numpy.float)                                                
                                                        #if len(finalDistr) == i and value is None:
                                                                #finalDistr.append([])
                                                        #elif len(finalDistr) == i and value is not None:
                                                                #finalDistr.append([value])
                                                        #elif value is None:
                                                                #continue
                                                        #else:
                                                                #finalDistr[i].append(value)
                                                                #finalDistr[i].append(1)
                                                                #finalDistr[i].append(value)



                                # transform the dictionary mgProfile in a numpy array 2D mgProfiles (the s makes the difference)
                                keys = mgProfile.keys()
                                keys.sort()
                                nProfiles = len(keys)
                                arrayLenProfile = numpy.asarray(lenProfile)
                                maxLenProfile = numpy.amax(arrayLenProfile)
                                mgProfiles = numpy.empty(shape=(nProfiles,maxLenProfile)) * numpy.nan #initialize all the vector to nan values    
                                # fill the matrix in different order according to "align" set (if align is equal to "begin" or equal to "end")
                                if align == "begin":                                
                                        for i,key in enumerate(keys):
                                                for pos in range(len(mgProfile[key])):
                                                        mgProfiles[i,pos] = mgProfile[key][pos]                                                   
                                                del mgProfile[key]
                                elif align == "end": 
                                        for i,key in enumerate(keys):
                                                for pos in range(1,len(mgProfile[key])+1): # pos goes from 1 to len+1 because the matrix is filled from the end to the vector till the beginning of the vector
                                                        mgProfiles[i,-pos] = mgProfile[key][-pos]                                                   
                                                del mgProfile[key]
                                else:
                                        raise TypeError("align set {} not recognized. Choose align value between 'begin' and 'end' of the interval\n".format(align))                                        
                                        
                                # remove the mgProfile dictionary
                                mgProfile.clear()
                                

                                #the outliers value are transformed in negative value, then in the calculation of the mean only the values greater than 0 are taken into account
                                if rmOutliers:
                                        for i in range(maxLenProfile):
                                        #for index, posDist in enumerate(finalDistr):
                                                #if len(posDist) == 0:
                                                if numpy.sum(mgProfiles[:,i]) == 0:
                                                        continue
                                                else:
                                                        mean = numpy.nanmean(mgProfiles[:,i])
                                                        sd = numpy.nanstd(mgProfiles[:,i])
                                                        mgProfiles[:,i] = [x if x <= float(mean) + 5 * float(sd) else -x for x in mgProfiles[:,i]]
                                                        #mean = numpy.mean(posDist)
                                                        #sd = numpy.std(posDist)
                                                        #finalDistr[index] = [x for x in posDist if x <= float(mean) + 5 * float(sd)]                                                        
                                        #print the outliers profiles (#TODO)

                                if printProfiles:
                                        outputProfiles = open(coordsName+"/"+wigName+"_profiles.csv", "w")
                                        for i,key in enumerate(keys):
                                                trash = key.split(":")
                                                outputProfiles.write(",".join(str(x) for x in trash) + "," + ",".join("" if numpy.isnan(x) else str(x) for x in mgProfiles[i]) + "\n") 
                                                #for further analysis one have to deal with NA values

##### da qui in poi vanno integrate le nuove modifiche, tenendo conto che deve funzionare come funzionava prima e che quando medio sui vettori numpy e voglio escludere i valori nan,
# cioe quelli non definiti, devo usare la funzione numpy.nanmean o qualcosa di simile di scipy.

                                outputFile = open(coordsName+"/"+wigName+".txt", "w")
                                meanVector = numpy.zeros(maxLenProfile)
                
                                if function == 'median':
                                        #meanVector = []
                                        #for posDist in finalDistr:
                                                #if len(posDist) < minValues:
                                                        #meanVector.append("")
                                                #else:
                                                        #meanVector.append(numpy.median(posDist))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        #outputFile.write(",".join(str(x) for x in meanVector))
                                        for i in range(len(meanVector)):
                                                valuableValues = [x for x in mgProfiles[:,i] if not numpy.isnan(x) and x > 0]
                                                if len(valuableValues) >= minValues:
                                                        meanVector[i] = numpy.median(valuableValues)
                                        outputFile.write(",".join("" if numpy.isnan(x) else str(x) for x in meanVector))
                                        outputFile.close()
                                elif function == 'mean':
                                        #meanVector = []
                                        #for posDist in finalDistr:
                                                #if len(posDist) < minValues:
                                                        #meanVector.append("")
                                                #else:
                                                        #meanVector.append(numpy.mean(posDist))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        #outputFile.write(",".join(str(x) for x in meanVector))
                                        for i in range(len(meanVector)):
                                                valuableValues = [x for x in mgProfiles[:,i] if not numpy.isnan(x) and x > 0]
                                                if len(valuableValues) >= minValues:
                                                        meanVector[i] = numpy.mean(valuableValues)
                                        outputFile.write(",".join("" if numpy.isnan(x) else str(x) for x in meanVector))
                                        outputFile.close()                                                
                                elif function == 'std':
                                        #meanVector = []
                                        #for posDist in finalDistr:
                                                #if len(posDist) < minValues:
                                                        #meanVector.append("")
                                                #else:
                                                        #meanVector.append(numpy.std(posDist))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        #outputFile.write(",".join(str(x) for x in meanVector))
                                        for i in range(len(meanVector)):
                                                valuableValues = [x for x in mgProfiles[:,i] if not numpy.isnan(x) and x > 0]
                                                if len(valuableValues) >= minValues:
                                                        meanVector[i] = numpy.std(valuableValues)
                                        outputFile.write(",".join("" if numpy.isnan(x) else str(x) for x in meanVector))
                                        outputFile.close()                                                 
                                elif function == 'sum':
                                        #meanVector = []
                                        #for posDist in finalDistr:
                                                #if len(posDist) < minValues:
                                                        #meanVector.append("")
                                                #else:
                                                        #meanVector.append(numpy.sum(posDist))  ###remove or add "/len(finalDistr[key])"after "sum(finalDistr[key])" if you want the sum of the mean respectively. sum(finalDistr[key])
                                        #outputFile.write(",".join(str(x) for x in meanVector))
                                        for i in range(len(meanVector)):
                                                valuableValues = [x for x in mgProfiles[:,i] if not numpy.isnan(x) and x > 0]
                                                if len(valuableValues) >= minValues:
                                                        meanVector[i] = numpy.sum(valuableValues)
                                        outputFile.write(",".join("" if numpy.isnan(x) else str(x) for x in meanVector))
                                        outputFile.close() 				


	
#if __name__ == '__main__':
#	


#	os.chdir("/Users/Tito/Programming/python/Rap1/shore_rap1_chipseq/metagene_rap1aa")
#	pls= Metagene(['acs_coords.txt', "acs_coords_rev.txt"], ["rna15c2normalizedWig/RNA1537C2_clipped_qtrimmed_collapsed_nonPa_revcomp_plus.wig","rna15c2normalizedWig/WT30C2_clipped_qtrimmed_collapsed_nonPa_revcomp_plus.wig"],["rna15c2normalizedWig/RNA1537C2_clipped_qtrimmed_collapsed_nonPa_revcomp_minus.wig","rna15c2normalizedWig/WT30C2_clipped_qtrimmed_collapsed_nonPa_revcomp_minus.wig"])
#	pls.analysis('median')


#	print 'pls'