import os
import string
import re


ara2Rom = {"1": "I",
           "2": "II",
           "3": "III",
           "4" : "IV",
           "5" : "V",
           "6" : "VI",
           "7" : "VII",
           "8" : "VIII",
           "9" : "IX",
           "10" : "X",
           "11": "XI",
           "12" : "XII",
           "13" : "XIII",
           "14" : "XIV",
           "15" : "XV",
           "16" : "XVI", 
           "17" : "MT"}   
rom2Ara = {"I" : "1",
           "II" : "2",
           "III" : "3",
           "IV" : "4",
           "V" : "5",
           "VI" : "6",
           "VII" : "7",
           "VIII" : "8",
           "IX" : "9",
           "X" : "10",
           "XI" : "11",
           "XII" : "12",
           "XIII" : "13",
           "XIV" : "14",
           "XV" : "15",
           "XVI" : "16",
           "MT": "17"}   


#----------------------------------------------------------------------
def chromConversion(chromList, roman = True):
	"""
	|  take a list and turn all the instances of chromosome notations into roman or arabic numeral, as per choice.
	|
	|  Arguments:
	|      chromlist: list of text, lines that do not contain chromosome notations will be left as is
	|      roman: True denotes roman numerals output, False denotes arabic numerals output
	"""



	ara2RomPattern = re.compile('chr0?(\d+)')
	rom2AraPattern = re.compile('chr([X|V|I|M|T]+)')
	returnList = []

	if roman:
		if type(chromList) is list:
			for i in chromList:
				i = ara2RomPattern.sub( lambda match: "chr" + ara2Rom[match.group(1)], i)
				returnList.append(i)
			return returnList
		else:
			chromList =  ara2RomPattern.sub(lambda match: "chr" + ara2Rom[match.group(1)], chromList)
			return chromList
	else:
		if type(chromList) is list:
			for i in chromList:
				i = rom2AraPattern.sub(lambda match: "chr"+rom2Ara[match.group(1)], i)
				returnList.append(i)
			return returnList
		else:
			chromList =  rom2AraPattern.sub( lambda match: "chr"+rom2Ara[match.group(1)], chromList)
			return chromList
		

#----------------------------------------------------------------------
def revComp(line, mask = False):
	"""
	|  Read a nucleotide sequence and return a string containing its reverse complement.
	|  
	|  Arguments:
	|      line: string representing the nucleotide sequence, newlines will be stripped.
	|      mask: masks non-ATGC nucleotides if True (default False).
	|
	|  Masked nucleotides are removed from the final returned string.

	"""
	line = str(line)
	line = line.rstrip("\n")

	#if mask is true, line retains only ATGC characters
	if mask:
		matches = re.findall("[A|T|C|G]+", line.upper())
		line = "".join(matches)

	lineReverse = line[::-1]  #reverse a string. use "extended slice" to do this (usually begin:end:step)
	lineReverseComplement = lineReverse.translate(string.maketrans("ACGTacgt", "TGCATGCA"))

	return lineReverseComplement

#----------------------------------------------------------------------
def fileToList(fileName):
	"""
	  Read a text file and return a list containing all lines. 
	
	  Arguments:
	      filename: path to the file to be opened
	"""
	if os.path.isfile(fileName):
		textFile = open(fileName)
	else:
		raise IOError('file {} not found.'.format(fileName))


	list = []

	for i in textFile:
		i = i.rstrip("\n")
		list.append(i)

	textFile.close
	return list