import numpy
import os
import bioutils
import re
import sys
import urllib


class Wig:
	"""Store a wig file as a series of nested dictionaries
	
	  Arguments:
	      path: path to the wig file to be imported
	      convertChrom: convert chromosome names into romans, default true
	
	  Attributes:
	      chromlist: list containing all the chromosomes in the wig file
	      _wig: datastructure containing the data composed of nested dictionaries.
	
	  Methods:
	        value: extract the value at a given position for a given chromosome
	        valuelist: extract the value for a range of positions for a given chromosome
	"""

	#----------------------------------------------------------------------
	def __init__(self, path, convertChrom = True,):
		"""Constructor for the wig class.
		parse the wig file and embed it in a datastructure of nested dictionaries.
		"""
	
		# initialize storing objects
		self._wig = {}
		self.chromList = []
		self.name = ""
	
		# initialize variables to parse the file
		wigHeaderPattern = re.compile("variableStep chrom=(chr)?0?([XVI]+|2-micron|Mito|[0-9]+|mt|mito|.+)( span=\d)?")  #TODO:ENABLE THE PARSING OF ARBITRARY CHROMOSOMES
		wigTuplePattern =  re.compile("^(\d+)\s(.+)$")
		wigFirstHeaderPattern = re.compile("track type=wiggle_0 name=(.+?) ")
		current_chr = None		
	
		# detect if the input it is a wig object (type=dict) or a wig file(type=str)
		if type(path) is dict:
			for chromo in path:
				if chromo is 'name':
					self.name = path['name']
				else:
					current_chr = chromo                
					if convertChrom:
						current_chr = bioutils.misc.chromConversion(current_chr)
					self._wig[current_chr] = path[chromo]
					self.chromList.append(current_chr)
	
	
		elif type(path) is str and os.path.exists(path):
			if os.path.isfile(path):
				wigFile = open(path)
			else:
				raise IOError('could not read file {}'.format(path))
	
			for line in wigFile:
				line = line.strip()
	
				#process tuple if found
				match = wigTuplePattern.search(line)
				if match:
					self._wig[current_chr][int(match.group(1))] = float(match.group(2))
					continue
	
				#process chr header if found
				match = wigHeaderPattern.search(line)
	
				if  match:
					if not match.group(1):
						current_chr = match.group(2)
						if convertChrom:
							current_chr = bioutils.misc.chromConversion(current_chr)
					else:
						current_chr = match.group(1) + match.group(2)
						if convertChrom:
							current_chr = bioutils.misc.chromConversion(current_chr)
					self._wig[current_chr] = {}
					self.chromList.append(current_chr)
					continue			
	
				#process wig header if found
				match = wigFirstHeaderPattern.search(line)
				if match:
					name = match.group(1)
					continue			
	
		else:
			raise TypeError("the wig class requires either a dictionary of values, a file name or a path to a file that exists. There was a problem with {}.".format(path))

	#----------------------------------------------------------------------
	def value(self, chromosome, position, default=None):
		"""
		  return the value at specified chromosome and position if present,
		  return None otherwise
		
		  Arguments:
		      chromosome: name of the chromosome containing the position of interest.
		      position: nucleotide position for wich the value is requested.
			  default: specifies the value to be returned if position does not have any value in the wig file.
		
		  Note: as in the wig file, the positions are 1 based, not 0 based.
		"""
		chromosome = str(chromosome)
		position = int(position)

		#checking arguments
		if chromosome not in self.chromList:
			raise ValueError("The provided chromosome is not present in the wig file.\n")        

		value = self._wig[chromosome].get(position, default)
		return value

	#----------------------------------------------------------------------
	def rangeValue(self, chromosome,  start,  stop,  default = None):
		"""
		  return the list of values present at all positions between start and stop (included)
		
		  Arguments:
		      chromosome: name of the chromosome containing the positions of interest
		      start: lower bound of the range of positions for which values will be reported, must be smaller than stop
		      stop: upper bound of the range of positions for which values will be reported, must be larger than start
		      default: if a position has no values, its position in the list will have this value
		"""
		valuesList =  []
		chromosome = str(chromosome)
		start = int(start)
		stop =  int(stop)

		#checking arguments
		if chromosome not in self.chromList:
			raise ValueError("provided chromosome argument to range function is not correct, it was {0}\n".format(chromosome))
		if start >= stop:
			raise ValueError("start value must be strictly lower than stop value, start was {0}, stop was {1}\n".format(start, stop))

		for position in range(start, stop + 1):
			m = self._wig[chromosome].get(position, None)

			if m:
				valuesList.append(float(m))
			else:
				valuesList.append(default)

		return valuesList
	
	#----------------------------------------------------------------------
	def summValue(self, coords, startOffset, endOffset, strand, func = 'median', minValues=3):
		"""provide a summary of values in a certain range (mean,median,sum)"""
		valuesDict = {}
		for chrom, coordStrand, start, end in coords.coordsIter():
			if strand == coordStrand and strand == "+":
				newStart = int(start) - int(startOffset)
				newEnd = int(end) + int(endOffset)
				rangeValues = self.rangeValue(chrom, newStart, newEnd)
				cleanRangeValues = [value for value in rangeValues if value is not None]
				if len(cleanRangeValues) >= minValues:
					if func == 'median':	
						summarizedValue = numpy.median(cleanRangeValues)
					elif func == 'mean':
						summarizedValue = numpy.mean(cleanRangeValues)
					elif func == "sum":
						summarizedValue = numpy.sum(cleanRangeValues)
					else:
						raise TypeError('argument provided to func was not among the possible options, only sum, mean and median allowed')
					valuesDict[":".join([chrom, coordStrand, start, end])] = summarizedValue
				else:
					continue
			if strand == coordStrand and strand == "-":
				newEnd = int(end) + int(startOffset)
				newStart = int(start) - int(endOffset)
				rangeValues = self.rangeValue(chrom, newStart, newEnd)
				cleanRangeValues = [value for value in rangeValues if value is not None]
				if len(cleanRangeValues) >= minValues:
					if func == 'median':	
						summarizedValue = numpy.median(cleanRangeValues)
					elif func == 'mean':
						summarizedValue = numpy.mean(cleanRangeValues)
					elif func == "sum":
						summarizedValue = numpy.sum(cleanRangeValues)
					else:
						raise TypeError('argument provided to func was not among the possible options, only sum, mean and median allowed')
					valuesDict[":".join([chrom, coordStrand, start, end])] = summarizedValue
				else:
					continue			
		return valuesDict
				

	#----------------------------------------------------------------------
	def subWig(self, wig):
		"""subtract the values of provided wig from the current one."""
		if not isinstance(wig, Wig):
			raise IOError("subWig requires an object of class Wig as input.\n")
		
		for chrom in self.chromList:
			#check that the selected chromosome is present on the other wig as well. 
			if chrom not in wig.chromList:
				sys.stderr.write("Chromosome {} was not found in the provided wig. No processing shall happen on this chromosome.\n")
				continue
			#iterate through all positions
			for position in range(1, max([int(i) for i in self._wig[chrom].keys()])):
				if position in self._wig[chrom] and position in wig._wig[chrom]:
					result = self.value(chrom, position) - wig.value(chrom, position)
					if result <= 0:
						self._wig[chrom].pop(position)
					else:
						self._wig[chrom][position] = result
		
		
	#----------------------------------------------------------------------
	def sumWig(self, wig):
		"""sum the values of provided wig from the current one."""
		if not isinstance(wig, Wig):
			raise IOError("sumWig requires an object of class Wig as input.\n")
		
		for chrom in self.chromList:
			#check that the selected chromosome is present on the other wig as well. 
			if chrom not in wig.chromList:
				sys.stderr.write("Chromosome {} was not found in the provided wig. No processing shall happen on this chromosome.\n".format(chrom))
				continue
			#iterate through all positions
			for position in range(1, max([int(i) for i in self._wig[chrom].keys()])):
				if position in self._wig[chrom] and position in wig._wig[chrom]:
					self._wig[chrom][position] = self.value(chrom, position) + wig.value(chrom, position)
				elif position in wig._wig[chrom]:
					self._wig[chrom][position] = wig.value(chrom, position)
				elif position in self._wig[chrom]:
					self._wig[chrom][position] = self.value(chrom, position)
				else:
					pass
		
	#----------------------------------------------------------------------
	def alphaWig(self, alpha):
		"""multiply the values of the given wig by the factor alpha."""
		alpha = float(alpha)
		
		for chrom in self.chromList:
			#iterate through all positions
			for position in range(1, max([int(i) for i in self._wig[chrom].keys()])):
				if position in self._wig[chrom]:
					self._wig[chrom][position] = self.value(chrom, position) * alpha
				else:
					pass
		
	#----------------------------------------------------------------------
	def writeWig(self, outputFile, name=""):
		"""write the wig object to a file"""
		if not name:
			name = self.name
		if os.path.isfile(outputFile):
			raise IOError("{} already exists! select another name.\n".format(outputFile))
		outFile = open(outputFile, 'w')
		outFile.write("track type=wiggle_0 name={} description={}\n".format(name, name))
		
		for chrom in self.chromList:
			outFile.write("variableStep chrom={}\n".format(chrom))
			for position in sorted(self._wig[chrom].keys(), key=int):
				outFile.write("{} {}\n".format(position, self._wig[chrom][position]))
		
					
		
########################################################################
class Coordinates:
	"""store a set of coordinates of the form chromosome:strand:start:stop
	
	  Arguments:
	      inputCoords: either a path to a file, a list or a string containing coordinates
	      conversion: can be either 'roman' or 'arab'.
	                  needed only if the coordinates need to be converted to the respective format
	
	  Attributes:
	      coordCount: the number of coordinates stored in the object
	      current: accessible only during iteration, represent the coordinate that is currently being iterated.
	      coordList: list of all the stored coordinates
	
	  Methods:
	      coordSplit: return a list containing the 4 components of the coordinate
	      next: used as iterator to iterate through the list. sets current.
	      bedFormat: formats the coordinates as bed and return a list
	"""
	#----------------------------------------------------------------------
	def __init__(self, inputCoords, conversion = None): 
		"""Constructor"""

		self.coordList = []
		self._iterable = 0
		self.current = None
		self.coordCount = None

		if type(inputCoords) is list:
			self.coordList = inputCoords
		elif type(inputCoords) is str and os.path.exists(inputCoords):
			self.coordList = bioutils.misc.fileToList(inputCoords)              
		else:
			raise TypeError("the coordinates class requires either a list of coordinates, a string or a path to a file that exists. there was a problem with {}.".format(inputCoords))

		coordinates_pat = re.compile("\s*(\w+?:[+|-]:\d+:\d+)\s*")
		indexes = range(len(self.coordList))
		if len(indexes) > 1:
			indexes.reverse()  #reverse is so that popping elements doesn't screw the iteration up. this section is bad.
		for i in indexes:
			match = coordinates_pat.search(self.coordList[i])
			if not match:
				self.coordList.pop(i)
			else:
				self.coordList[i] = match.group(1)

		self.coordCount = len(self.coordList)    

		if conversion == "arab":
			self.coordList = bioutils.misc.chromConversion(self.coordList, roman=False)
		elif conversion == "roman":
			self.coordList = bioutils.misc.chromConversion(self.coordList)
	#----------------------------------------------------------------------
	def coordsIter(self):
		"""iterate through the coordinates, return the 4 elements of the coordinate"""
		for coord in self.coordList:
			chrom, strand, start, stop = coord.split(":")
			yield chrom, strand, start, stop 

	#----------------------------------------------------------------------
	def coordSplit(self):
		"""return a list containing the 4 components of the coordinate
		  if current is not defined, returns a 2d list with the whole set of coords split"""

		splitList = []

		for i in self.coordList:
			splitList.append(i.split(":"))

		return splitList


	#----------------------------------------------------------------------
	def bedFormat(self, outFile=None):  
		"""format the coordinate as bed
		
		  Arguments:
		      name: either a string with one name or a list of names for every coordinate, embedded into the bed
		            formatted string.
		"""
		returnList = []
		counter = 0
		if outFile:
			output = open(outFile, "w")
			
			for chrom, strand, start, end in self.coordsIter():
				counter += 1
				start = int(start) - 1
				output.write("{0}\t{1}\t{2}\t{3}\t.\t{4}\n".format(chrom, start, end, str(counter), strand))
			return 1
		else:
			for chrom, strand, start, end in self.coordsIter():
				counter += 1
				start = int(start) - 1
				returnList.append("{0}\t{1}\t{2}\t{3}\t.\t{4}".format(chrom, start, end, str(counter), strand))
			return returnList

	#----------------------------------------------------------------------
	def writeCoords(self, outputFile):
		"""write the coords object to a file"""
	
		if os.path.isfile(outputFile):
			raise IOError("{} already exists! select another name.\n".format(outputFile))
	
		outFile = open(outputFile, 'w')
	
		for line in self.coordList:			
			outFile.write("{}\n".format(line))
		outFile.close()

#======================================================================
########################################################################
class CoordinatesWithName:
	"""store a set of coordinates of the form chromosome:strand:start:stop:featureValue:name
	
	  Arguments:
	      inputCoords: either a path to a file, a list or a string containing coordinates
	      conversion: can be either 'roman' or 'arab'.
	                  needed only if the coordinates need to be converted to the respective format
	
	  Attributes:
	      coordCount: the number of coordinates stored in the object
	      current: accessible only during iteration, represent the coordinate that is currently being iterated.
	      coordList: list of all the stored coordinates
	
	  Methods:
	      coordSplit: return a list containing the 6 components of the coordinate
	      next: used as iterator to iterate through the list. sets current.
	      bedFormat: formats the coordinates as bed and return a list
	"""
	#----------------------------------------------------------------------
	def __init__(self, inputCoords, conversion = None): 
		"""Constructor"""

		self.coordList = []
		self._iterable = 0
		self.current = None
		self.coordCount = None

		if type(inputCoords) is list:
			self.coordList = inputCoords
		elif type(inputCoords) is str and os.path.exists(inputCoords):
			self.coordList = bioutils.misc.fileToList(inputCoords)              
		else:
			raise TypeError("the coordinates class requires either a list of coordinates, a string or a path to a file that exists. there was a problem with {}.".format(inputCoords))

		# This string match also the chromosome 2-micron and as a feature word but also float number, as a name also name that contains word, numbers, % ans -
		coordinates_pat = re.compile("\s*(\d?-?\w+?:[+|-]:\d+:\d+:\w+(\.\d+)?\w?[+|-]?\d*:[\w|\%|-]+)\s*")
		indexes = range(len(self.coordList))
		if len(indexes) > 1:
			indexes.reverse()  #reverse is so that popping elements doesn't screw the iteration up. this section is bad.
		for i in indexes:
			match = coordinates_pat.search(self.coordList[i])
			if not match:
				self.coordList.pop(i)
			else:
				self.coordList[i] = match.group(1)

		self.coordCount = len(self.coordList)    

		if conversion == "arab":
			self.coordList = bioutils.misc.chromConversion(self.coordList, roman=False)
		elif conversion == "roman":
			self.coordList = bioutils.misc.chromConversion(self.coordList)
	#----------------------------------------------------------------------
	def coordsIter(self):
		"""iterate through the coordinates, return the 6 elements of the coordinate"""
		for coord in self.coordList:
			chrom, strand, start, stop, featureValue, name = coord.split(":")
			yield chrom, strand, start, stop, featureValue, name

	#----------------------------------------------------------------------
	def coordSplit(self):
		"""return a list containing the 6 components of the coordinate
		  if current is not defined, returns a 2d list with the whole set of coords split"""

		splitList = []

		for i in self.coordList:
			splitList.append(i.split(":"))

		return splitList


	#----------------------------------------------------------------------
	def bedFormat(self, outFile=None):  
		"""format the coordinate as bed
		
		  Arguments:
		      name: either a string with one name or a list of names for every coordinate, embedded into the bed
		            formatted string.
		"""
		# ToDo integrate the name and feature in the bed file
		returnList = []
		counter = 0
		if outFile:
			output = open(outFile, "w")
			
			for chrom, strand, start, end, featureValue, name in self.coordsIter():
				counter += 1
				start = int(start) - 1
				output.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format(chrom, start, end, name, featureValue, strand))
			return 1
		else:
			for chrom, strand, start, end, featureValue, name in self.coordsIter():
				counter += 1
				start = int(start) - 1
				returnList.append("{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(chrom, start, end, name, featureValue, strand))
			return returnList

	#----------------------------------------------------------------------
	def writeCoords(self, outputFile):
		"""write the coords object to a file"""
	
		if os.path.isfile(outputFile):
			raise IOError("{} already exists! select another name.\n".format(outputFile))
	
		outFile = open(outputFile, 'w')
	
		for line in self.coordList:			
			outFile.write("{}\n".format(line))
		outFile.close()

#======================================================================
########################################################################
class CoordinatesWithSeq:
	"""store a set of coordinates of the form chromosome:strand:start:stop:sequence
	
	  Arguments:
	      inputCoords: either a path to a file, a list or a string containing coordinates
	      conversion: can be either 'roman' or 'arab'.
	                  needed only if the coordinates need to be converted to the respective format
	
	  Attributes:
	      coordCount: the number of coordinates stored in the object
	      current: accessible only during iteration, represent the coordinate that is currently being iterated.
	      coordList: list of all the stored coordinates
	
	  Methods:
	      coordSplit: return a list containing the 6 components of the coordinate
	      next: used as iterator to iterate through the list. sets current.
	      bedFormat: formats the coordinates as bed and return a list
	"""
	#----------------------------------------------------------------------
	def __init__(self, inputCoords, conversion = None): 
		"""Constructor"""

		self.coordList = []
		self._iterable = 0
		self.current = None
		self.coordCount = None

		if type(inputCoords) is list:
			self.coordList = inputCoords
		elif type(inputCoords) is str and os.path.exists(inputCoords):
			self.coordList = bioutils.misc.fileToList(inputCoords)              
		else:
			raise TypeError("the coordinates class requires either a list of coordinates, a string or a path to a file that exists. there was a problem with {}.".format(inputCoords))

		coordinates_pat = re.compile("\s*(\w+?:[+|-]:\d+:\d+:\w+)\s*")
		indexes = range(len(self.coordList))
		if len(indexes) > 1:
			indexes.reverse()  #reverse is so that popping elements doesn't screw the iteration up. this section is bad.
		for i in indexes:
			match = coordinates_pat.search(self.coordList[i])
			if not match:
				self.coordList.pop(i)
			else:
				self.coordList[i] = match.group(1)

		self.coordCount = len(self.coordList)    

		if conversion == "arab":
			self.coordList = bioutils.misc.chromConversion(self.coordList, roman=False)
		elif conversion == "roman":
			self.coordList = bioutils.misc.chromConversion(self.coordList)
	#----------------------------------------------------------------------
	def coordsIter(self):
		"""iterate through the coordinates, return the 5 elements of the coordinate"""
		for coord in self.coordList:
			chrom, strand, start, stop, sequence = coord.split(":")
			yield chrom, strand, start, stop, sequence

	#----------------------------------------------------------------------
	def coordSplit(self):
		"""return a list containing the 6 components of the coordinate
		  if current is not defined, returns a 2d list with the whole set of coords split"""

		splitList = []

		for i in self.coordList:
			splitList.append(i.split(":"))

		return splitList


	#----------------------------------------------------------------------
	def bedFormat(self, outFile=None):  
		"""format the coordinate as bed
		
		  Arguments:
		      name: either a string with one name or a list of names for every coordinate, embedded into the bed
		            formatted string.
		"""
		# ToDo integrate the name and feature in the bed file
		returnList = []
		counter = 0
		if outFile:
			output = open(outFile, "w")
			
			for chrom, strand, start, end, sequence in self.coordsIter():
				counter += 1
				start = int(start) - 1
				output.write("{0}\t{1}\t{2}\t{3}\t.\t{4}\n".format(chrom, start, end, str(counter), strand))
			return 1
		else:
			for chrom, strand, start, end, sequence in self.coordsIter():
				counter += 1
				start = int(start) - 1
				returnList.append("{0}\t{1}\t{2}\t{3}\t.\t{4}".format(chrom, start, end, str(counter), strand))
			return returnList

	#----------------------------------------------------------------------
	def writeCoords(self, outputFile):
		"""write the coords object to a file"""
	
		if os.path.isfile(outputFile):
			raise IOError("{} already exists! select another name.\n".format(outputFile))
	
		outFile = open(outputFile, 'w')
	
		for line in self.coordList:			
			outFile.write("{}\n".format(line))
		outFile.close()


#======================================================================
########################################################################
class Fasta:
	"""
	|  read a fasta file or take a list of sequences and headers.
	|
	|  Arguments:
	|      filename: path to the file to be read, defaults to None
	|      headers: list of strings to put as fasta headers, must be a list of length equal to sequences
	|      sequences: list of sequences to be included in the fasta file, must be a list of length equal to headersw
	|      enforce: enforces the well-formedness of a fasta when reading form file. e.g. headers and sequences must have a 1:1 ratio.
	|
	|  Attributes:
	|     sequencelist: list of all the DNA sequences
	|     headerlist: list of all the headers if present
	|     sequencecount: number of sequences present in the object
	|
	|  Methods:
	|      write: write the contents of the object in fasta format to specified file
	|
	"""

	#----------------------------------------------------------------------
	def __init__(self, filename = None, headers = None, sequences = None, enforce = True):
		"""Constructor"""

		self.sequenceList = []
		self.headerList = []        
		self.sequenceCount = None
		self._sequenceTemp = ''
		self.fastaDict = {}

		if filename:
			try:
				textfile = open(filename)
			except IOError as e:
				_IOExceptionHandler()

			for i in textfile:
				i = i.rstrip("\n")
				if re.match("^$", i): continue
				if re.match("^>", i):
					self.headerList.append(i[1:])
					if self._sequenceTemp:
						self.sequenceList.append(self._sequenceTemp.upper())
					self._sequenceTemp = ''
					continue

				self._sequenceTemp = self._sequenceTemp + i
				# match = re.findall("[ATCG]+", i.upper())
				# if match:
				#     if mask:
				#         self.sequenceList.append("".join(match.group(0)))
				#     else:
				#         self.sequenceList.append(i.upper())

			textfile.close

			self.sequenceList.append(self._sequenceTemp.upper())
		else:
			self.sequenceList = sequences
			self.headerList = headers            

		#enforcement  
		if enforce:
			if not len(self.sequenceList) == len(self.headerList):
				raise ValueError("this fasta file is not well formed; the number of headers and sequences is different\n")
			elif len(self.sequenceList) == 0:
				raise TypeError("no filename provided and headers and sequences are somehow wrong")

		self.sequenceCount = len(self.sequenceList)

		#create dictionary
		for i in range(len(self.headerList)):
			self.fastaDict[self.headerList[i]] = self.sequenceList[i]        

	#----------------------------------------------------------------------
	def write(self, fileName, index = None):
		"""print the contents of the object to filename
		|
		|  Arguments:
		|      filename: name of the file to print to.
		|      index: indexes of the sequences to print
		"""


		try:
			write = open(fileName, w)
		except IOError:
			_IOExceptionHandler()

		if index:
			for i in index:
				write.write("{0}\n{1}\n".format(self.headerList[i], self.sequenceList[i]))
		else:
			for i in range(self.sequenceCount):
				write.write("{0}\n{1}\n".format(self.headerList[i], self.sequenceList[i]))

	#------------------------------------------------------------------------
	def seqEstractor(self, header, start, end, reverseComp = False, oneBasedCoords = True):
		"""extract sequences given a set of coordinates and the header of a stored fasta sequence"""
		#option and error management
		if header not in self.fastaDict:
			raise TypeError("Provided header {0} does not match the current fasta file.".format(header))
		if oneBasedCoords:
			start = int(start) - 1
			end = int(end) - 1
		if start > end:
			raise TypeError("start must be strictly lower than end")
		#sequence extraction
		headerSequence = self.fastaDict[header]
		extractedSequence = headerSequence[start:end+1]  #the +1 is a consequence of how pytho handles slicing: start through end-1
		if reverseComp:
			return revcomp(extractedSequence)
		else:
			return extractedSequence


class Gff:
	"""read gff and structure it as nested dictionaries and lists.
	providing the path to a fasta file for the genome also ensures that the sequences of every feature will be stored. this is optional"""

	#----------------------------------------------------------------------
	def __init__(self, path, genomeFasta = None):
		"""Constructor"""
		#variable definition
		self.features = {}
		self.subFeatures = {}

		#file reading
		gff = open(path)
		if genomeFasta:
			genomeSeq = bioutils.parse.Fasta(genomeFasta)
		#gff parsing
		for line in gff:
			line = line.rstrip()
			if line.startswith("#") or line == "":
				continue
			fields = line.split("\t")
			chromosome = fields[0]
			source = fields[1]
			featureType = fields[2]
			start = fields[3]
			stop = fields[4]
			strand = fields[6]
			phase = fields[7]
			attributes = fields[8]

			#attribute processing
			attributesDict = {}
			attributesList = attributes.split(";")
			for attribute in attributesList:
				name, value = attribute.split("=")
				attributesDict[name] = urllib.unquote(value)  #urllib used to get notes to work properly


			#fill features or subFeatures dictionaries
			if "ID" in attributesDict:
				#initializes subfeatures list, so even IDs that have no subfeatures will have an empty set of subfeatures and not nothing
				self.subFeatures[attributesDict["ID"]] = []
				#fill stuff present in columns of gff
				self.features[attributesDict["ID"]] = {}
				self.features[attributesDict["ID"]]['chromosome'] = chromosome
				self.features[attributesDict["ID"]]['source'] = source
				self.features[attributesDict["ID"]]['type'] = featureType
				self.features[attributesDict["ID"]]['start'] = start
				self.features[attributesDict["ID"]]['end'] = stop
				self.features[attributesDict["ID"]]['strand'] = strand
				self.features[attributesDict["ID"]]['unprocessed attributes'] = attributes
				if genomeFasta:
					if strand == "-":
						self.features[attributesDict["ID"]]["antisense sequence"] = genomeSeq.seqEstractor(chromosome, start, stop)
						self.features[attributesDict["ID"]]['sense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop, reverseComp=True)                    
					else:  #strand == "+" or nostrand
						self.features[attributesDict["ID"]]['sense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop)
						self.features[attributesDict["ID"]]['antisense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop, reverseComp=True)

				#fill all attributes procedurally
				for attribute in attributesDict:
					if attribute == "ID":
						continue
					self.features[attributesDict["ID"]][attribute] = attributesDict[attribute]
			elif "Parent" in attributesDict:
				if not self.subFeatures[attributesDict["Parent"]]:
					#create list of dicts
					self.subFeatures[attributesDict["Parent"]] = []
				#create new dictionary as item of list of length index
				index = len(self.subFeatures[attributesDict["Parent"]]) #index to use in the list of features, basically counts how many features are already present in the list.
				self.subFeatures[attributesDict["Parent"]].append({}) 
				#populate dictionaries
				self.subFeatures[attributesDict["Parent"]][index]['chromosome'] = chromosome
				self.subFeatures[attributesDict["Parent"]][index]['source'] = source
				self.subFeatures[attributesDict["Parent"]][index]['type'] = featureType
				self.subFeatures[attributesDict["Parent"]][index]['start'] = start
				self.subFeatures[attributesDict["Parent"]][index]['end'] = stop
				self.subFeatures[attributesDict["Parent"]][index]['strand'] = strand
				self.subFeatures[attributesDict["Parent"]][index]['phase'] = phase
				self.subFeatures[attributesDict["Parent"]][index]['unprocessed attributes'] = attributes
				if genomeFasta:
					if strand == '-':
						self.subFeatures[attributesDict["Parent"]][index]['antisense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop)
						self.subFeatures[attributesDict["Parent"]][index]['sense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop, reverseComp=True)                    
					else:  #strand == "+" or no strand  
						self.subFeatures[attributesDict["Parent"]][index]['sense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop)
						self.subFeatures[attributesDict["Parent"]][index]['antisense sequence'] = genomeSeq.seqEstractor(chromosome, start, stop, reverseComp=True)

				#fill all attributes procedurally
				for attribute in attributesDict:
					if attribute == "parent":
						continue
					self.subFeatures[attributesDict["Parent"]][index][attribute] = attributesDict[attribute]
					
					
					
#if __name__ == '__main__':
					
					
					
	#os.chdir("/Users/Tito/Programming/python/replication")
	



	

